import axios from "axios"
import {  useRef, useState } from "react";

function App() {
  const [value,setValue] = useState(0);
  const [values,setValues] = useState([]);
  const inputRef = useRef();
  const handleClick = async () => {
      inputRef.current.value && setValue(inputRef.current.value)

        const res = await axios.get('https://11yhlbx7bb.execute-api.us-east-2.amazonaws.com/default/myFunction?n='+value)
        setValues(res.data)


 
 
  }


  return (
    <div className="App"  >
      <div className="cc">
      <h2>How many values you want to generate (between 0 and 9):</h2>
      <div>
      <input type="number"      ref={inputRef} /> 
      <input type="button" onClick={handleClick} value="Generate" />
      <h2>Random generated values:</h2>
      {
                values.map((v,i) => (
<p key={i} >{v}</p>
                ))
            }

      </div>
      
      </div>
     </div>
  );
}

export default App;
